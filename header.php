<?php $Drs = new Drs(); ?>
<!DOCTYPE html>
<!-- add a class to the html tag if the site is being viewed in IE, to allow for any big fixes -->
<!--[if lt IE 8]><html class="ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 9]><html class="ie9" <?php language_attributes(); ?>><![endif]-->
<!--[if gt IE 9]><html <?php language_attributes(); ?>><![endif]-->
<!--[if !IE]><html <?php language_attributes(); ?>><![endif]-->
<html <?php language_attributes(); ?> class="no-js">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <title>
        <?php 
        if (is_front_page()) {
            echo 'Psiquiatria em São Paulo - Dr. Fernando Sauerbronn Gouvea';
        }
        else {
            echo $Drs->page_title(); 
        }
        ?>
    </title>

    <meta name="author" content="" />
    <meta name="copyright" content="" />
    <meta name="abstract" content="" />
    <meta name="creator" content="" />

    <link rel="icon" href="favicon.ico" />
    <!--[if IE]><link rel="shortcut icon" href="favicon.ico"><![endif]-->

    <!-- Cdm's -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/styles-2.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
            <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.min.js"></script>
            <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
        <![endif]-->
    <?php wp_head(); ?>


    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NQDTXXV');</script>
<!-- End Google Tag Manager -->



</head>

<body <?php body_class('appear-animate'); ?> itemscope="" itemtype="http://schema.org/WebPage">


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NQDTXXV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="page-loader">
        <div class="loader">Loading...</div>
    </div>

    <header id="header" class="header">
        <span id="topo"></span>
        <div class="container">
            <div class="row">
                <button class="mobile--open-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <nav id="mobile--menu" class="mobile--menu">
                    <ul>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( home_url() ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(2); ?></a> <!-- Menu Home -->
                        </li>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( get_permalink(64) ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(64); ?>
                                <!-- Menu Sobre -->
                            </a>
                        </li>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( get_permalink(77) ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(77); ?>
                                <!-- Menu Doenças -->
                            </a>
                        </li>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( get_permalink(83) ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(83); ?>
                                <!-- Menu Tratamentos -->
                            </a>
                        </li>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( get_permalink(36) ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(36); ?>
                                <!-- Menu Perguntas e Respostas -->
                            </a>
                        </li>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( get_permalink(8) ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(8); ?>
                                <!-- Menu Blog -->
                            </a>
                        </li>
                        <li class="menu-mobile--item">
                            <a href="<?php echo esc_url( get_permalink(10) ); ?>" class="menu-mobile--link">
                                <?php echo get_the_title(10); ?>
                                <!-- Menu Contato -->
                            </a>
                        </li>
                    </ul>
                </nav>

                <nav class="header--menu">
                    <ul>
                        <li class="header--menu--item">
                            <a href="<?php echo esc_url( get_permalink(64) ); ?>" class="header--menu--link">
                                <?php echo get_the_title(64); ?>
                                <!-- Menu Sobre -->
                            </a>
                        </li>
                        <li class="header--menu--item">
                            <a href="<?php echo esc_url( get_permalink(77) ); ?>" class="header--menu--link">
                                <?php echo get_the_title(77); ?>
                                <!-- Menu doenças -->
                            </a>
                        </li>
                        <li class="header--menu--item">
                            <a href="<?php echo esc_url( get_permalink(83) ); ?>" class="header--menu--link">
                                <?php echo get_the_title(83); ?>
                                <!-- Menu Tratamentos -->
                            </a>
                        </li>
                    </ul>
                </nav>

                <a href="<?php echo esc_url( home_url() ); ?>" class="header--logo">
                    <h1>
                        <?php wp_title(); ?>
                    </h1> <!-- Menu home -->
                </a>

                <nav class="header--menu">
                    <ul>
                        <li class="header--menu--item">
                            <a href="<?php echo esc_url( get_permalink(8) ); ?>" class="header--menu--link">
                                <?php echo get_the_title(8); ?>
                                <!-- Menu Blog -->
                            </a>
                        </li>
                        <li class="header--menu--item">
                            <a href="<?php echo esc_url( get_permalink(10) ); ?>" class="header--menu--link">
                                <?php echo get_the_title(10); ?>
                                <!-- Menu Contato -->
                            </a>
                        </li>
                        <li class="header--menu--item phone-number">
                            <a href="tel:1134876597" class="header--menu--link phone-head">
                                (11) 3487-6597  / 
                            </a>
                            <a href="wpp" class="header--menu--link cellphone-head">
                                (11) 95047-6969
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- <button class="header--open-search">
                    <i class="fa fa-search"></i>
                </button>
                <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="header--search">
                    <input type="search" name="s" id="busca" placeholder="O que procura?" />
                    <input type="hidden" name="tipo-busca" value="any" />
                </form> -->
            </div>
        </div>
    </header>