<?php get_header(); ?>

	<main class="interna">
		<section class="interna--banner" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<h2 class="interna--banner-title"><?php echo __( 'Oops!', THEME_NAME ); ?></h2>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5">
						<div class="interna--banner-text"><?php echo __( 'Página não encontrada.', THEME_NAME ); ?></div>
						<a href="#interna-content" class="interna--banner-button scroll"><i class="fa fa-angle-down"></i></a>
					</div>
				</div>
			</div>
		</section>
		<section id="interna-content" class="container-fludi interna--contain">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
							<p><?php echo __( 'Parece que nada foi encontrado neste local. Talvez tente uma pesquisa.', THEME_NAME ); ?></p>
							<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>

<?php get_footer(); ?>
