<?php 
/*
*	Template Name: Perguntas
*/
?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="interna">
			<section class="interna--banner" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h2 class="interna--banner-title"><?php the_title(); ?></h2>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-5">
							<div class="interna--banner-text"><?php the_excerpt(); ?></div>
							<a href="#interna-content" class="interna--banner-button scroll"><i class="fa fa-angle-down"></i></a>
						</div>
					</div>
				</div>
			</section>
			<section id="interna-content" class="container-fludi interna--contain">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">

								<?php the_content(); ?>
								
								<?php 					
				                    $args = array(
				                        'post_type'      => 'perguntas',
				                        'orderby'        => 'menu_order', 
				                        'order'          => 'ASC',
				                        'posts_per_page' => -1
				                    );
				                   	$perguntas = new WP_Query( $args );

				                   	$n = 1;
								?>	
								<?php if($perguntas->have_posts()) : ?>
									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
										<?php while($perguntas->have_posts()) : $perguntas->the_post(); ?>
											<div class="panel panel-default">
												<div class="panel-heading" role="tab" id="heading-<?php echo $n; ?>">
													<h4 class="panel-title">
														<a role="button" class="<?php if($n > 1){ echo 'collapsed'; } ?>" data-toggle="collapse" data-parent="#accordion" href="#pergunta-<?php echo $n; ?>" aria-expanded="true" aria-controls="pergunta-<?php echo $n; ?>">
															<i class="fa fa-angle-down"></i>
															<?php the_title(); ?>
														</a>
													</h4>
												</div>
												<div id="pergunta-<?php echo $n; ?>" class="panel-collapse collapse<?php if($n == 1){ echo ' in'; } ?>" role="tabpanel" aria-labelledby="heading-<?php echo $n; ?>">
													<div class="panel-body">
														<?php the_content(); ?>
													</div>
												</div>
											</div>
										<?php $n++; endwhile; ?>
									</div>
								<?php endif; wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>