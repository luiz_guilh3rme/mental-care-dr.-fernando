<?php 
/*
*	Template Name: Login
*/

?>

<?php get_header(); ?>

	<section class="container-fluid">
		
		<div class="row">
			
			<div class="envolve-login">
				<div class="caixa-login">

					<h2><?php the_title(); ?></h2>

		            <?php the_content(); ?>
		            
	                <form id="login" method="post" class="login_">

                        <div class="form-row">
                            <input type="text" class="input-text" name="username" id="username" placeholder="<?php _e('+ Usuário', THEME_NAME); ?>" />
                        </div>

                        <div class="form-row">
                            <input class="input-text" type="password" name="password" id="password" placeholder="<?php _e('+ Senha', THEME_NAME); ?>" />
                        </div>

                        <div class="form-row">
                            <input type="submit" class="input-submit" name="login" value="<?php esc_attr_e( 'Login', THEME_NAME ); ?>"/>
                        </div>

                        <div class="form-row">
							<a class="lost small" href="<?php echo wp_lostpassword_url(); ?>"><?php _e('Esqueceu a senha?' , THEME_NAME); ?></a>
                        </div>

                        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>

                        <div id="carregando">
                        	<p class="status"></p>
                        	<img src="<?php bloginfo('template_url') ?>/images/ring.svg" alt="" />
                        </div>
                    </form>			            
			            
				</div>
			</div>

		</div>

	</section>

<?php get_footer(); ?>