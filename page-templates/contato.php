<?php 
/*
*	Template Name: Contato
*/
?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<main class="interna">
			<section class="interna--banner" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h2 class="interna--banner-title"><?php the_title(); ?></h2>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-5">
							<div class="interna--banner-text"><?php the_excerpt(); ?></div>
							<a href="#interna-content" class="interna--banner-button scroll"><i class="fa fa-angle-down"></i></a>
						</div>
					</div>
				</div>
			</section>
			<section id="interna-content" class="container-fludi interna--contain">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-offset-1 col-sm-5 col-md-5">
								
								<?php the_content(); ?>								

								<?php if(get_field('titulo_consultorio')): ?>
									<h3 class="title-icon">
										<i class="fa fa-home"></i>
										<?php echo get_field('titulo_consultorio'); ?>
									</h3>
								<?php endif; ?>
								<?php if(get_field('endereco_do_consultorio')): ?>
									<p class="end">
										<?php echo get_field('endereco_do_consultorio'); ?>
									</p>
								<?php endif; ?>
								
								<?php if(get_field('titulo_telefone')): ?>
									<h3 class="title-icon">
										<i class="fa fa-phone"></i>
										<?php echo get_field('titulo_telefone'); ?>
									</h3>
								<?php endif; ?>
								<?php if(get_field('telefone')): ?>
									<p class="end">
										<?php 					 
											$tel = str_replace(array( '(', ')' ), '', get_field('telefone'));
											$tel = str_replace(' ', '', $tel);
											$tel = str_replace('-', '', $tel);						
											echo '<a href="tel:'.$tel.'">'.get_field('telefone').'</a>';
										?>
									</p>
								<?php endif; ?>
							</div>
							<div class="col-xs-12 col-sm-5 col-md-5">
								<div class="form-contato">
									<?php 
										$form = get_field('adicionar_formulario');

										if(get_field('adicionar_formulario')){
											echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->ID.'" html_name="'.$form->post_name.'" html_id="contact-form-'.$form->ID.'"]');
										}				
									?>
								</div>
							</div>

							<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
								<?php if( get_field('adicionar_google_maps') ) : ?>
									<?php 
										$arrayMap = get_field('adicionar_google_maps');
										$address  = $arrayMap['address'];
										$lat      = $arrayMap['lat'];
										$lng      = $arrayMap['lng'];
									?>
									<div class="mapa">
										<div id="mapa" class="mapa" data-marker="<?php echo get_field('adicionar_pin_google_maps'); ?>" data-address="<?php echo $address; ?>"></div>
									</div>
								<?php endif; ?>	
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

	<?php endwhile; ?>

<?php get_footer(); ?>