<?php get_header(); ?>
	
	<?php while ( have_posts() ) : the_post(); ?>
		<main id="single-<?php the_ID(); ?>" <?php post_class('interna'); ?>>
			<section class="interna--banner" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h2 class="interna--banner-title"><?php the_title(); ?></h2>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-5">
							<a href="#interna-content" class="interna--banner-button scroll"><i class="fa fa-angle-down"></i></a>
						</div>
					</div>
				</div>
			</section>
			<section id="interna-content" class="container-fludi interna--contain">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
								
								<?php the_content(); ?>
								
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
	<?php endwhile; ?>

<?php get_footer(); ?>