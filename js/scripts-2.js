(function ($) {
    "use strict";
    $(document).ready(function () {
        //Caroseis
        var owlDoencas = $(".carousel-doencas");
        owlDoencas.owlCarousel({
            items: 3,
            itemsDesktop: [1000, 3],
            itemsDesktopSmall: [900, 3],
            itemsTablet: [600, 2],
            itemsMobile: [480, 1],
            navigation: true,
            pagination: false
        });
    });

})(jQuery);

window.onload = function () {
    function openModal() {
        $('.modal').fadeIn(400, function(){
            $('.form-wrapper').fadeIn();
        });
    }

    function closeModal() {
        $(this).parent('.form-wrapper').fadeOut(400, function () {
            $(this).parent('.modal').fadeOut();
        })
    }
    $('.close-modal').on('click', closeModal);
    $('.open-modal').on('click', openModal);

    console.log('window loaded');

    $('.close-zap').on('click', function(){
        $(this).parent('.whatsapp').addClass('hidden');
    });
}