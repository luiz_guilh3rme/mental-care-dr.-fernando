<?php

define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URI', get_template_directory_uri() );

define( 'THEME_NAME', 'drs' );
define( 'THEME_VERSION', '1.0' );

define( 'INC_DIR',  THEME_DIR. '/inc' );
define( 'LANG_DIR', THEME_DIR. '/languages' );

class Drs{

    function __construct() {

        add_action( 'wp_enqueue_scripts', array($this, 'init_enqueue_ajax_login') ); // Enqueue Scripts
        add_action( 'wp_ajax_nopriv_ajaxlogin', array($this, 'init_ajax_login') );
        
        add_action( 'init', array($this, 'add_excerpts_to_pages' ));

        add_action( 'widgets_init', array($this, 'init_sidebars') );

        add_action( 'wp_head', array($this, 'uri_path_script') );

        add_action( 'init', array($this, 'init_register_nav' ));

        add_filter( 'post_thumbnail_html', array($this, 'remove_thumbnail_dimensions'), 10 );
        add_filter( 'image_send_to_editor', array($this, 'remove_thumbnail_dimensions'), 10 );

        add_filter('acf/fields/google_map/api', array($this, 'my_acf_google_map_api'));
        
        // Add theme support for woocommerce plugin
        add_theme_support( 'woocommerce' );

        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'post-formats', array( 'status', 'quote', 'gallery', 'image', 'video', 'audio', 'link', 'aside', 'chat' ) );
        add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

        // Add theme support for Translation
        load_theme_textdomain( THEME_NAME, LANG_DIR );

        /** Shortcodes Filters */
        add_filter( 'widget_text', 'do_shortcode' );
        add_filter( 'category_description', 'do_shortcode' );
            
        // Featured Image
        set_post_thumbnail_size( 260,  146,  false );
        
        // Custom Post Format - List Thumbnails
        add_image_size( 'thumb-blog', 376, 221, true );
        add_image_size( 'post_type', 80,   50,   true );
    

        show_admin_bar(false);

        require_once( INC_DIR . '/init.php' );
    }


    /* Setup Theme */

    public function init_register_nav() {

        $locations = array(
            'primary'   => __( 'Menu Header', THEME_NAME ),
            'footer'    => __( 'Menu Footer', THEME_NAME ),
            'mobile'    => __( 'Menu Mobile', THEME_NAME ),
        );
        register_nav_menus( $locations );

    }

    public function init_sidebars() {
        $args = array(
            'id'            => 'default',
            'name'          => __( 'Padrão', THEME_NAME ),
            'description'   => __( 'Adicionar widgets ao sidebar padrão do tema.', THEME_NAME ),
            'before_widget' => '<section id="default">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        );
        register_sidebar( $args );

        $args = array(
            'id'            => 'call-to-action',
            'name'          => __( 'Call to action', THEME_NAME ),
            'description'   => __( 'Adicionar widgets ao sidebar padrão do tema.', THEME_NAME ),
            'before_widget' => '<section class="call-to-action">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        );
        register_sidebar( $args );

        $args = array(
            'id'            => 'footer',
            'name'          => __( 'Footer', THEME_NAME ),
            'description'   => __( 'Adicionar widgets ao footer do tema.', THEME_NAME ),
            'before_widget' => '<section id="footer">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        );
        register_sidebar( $args );

        $args = array(
            'id'            => 'copyright',
            'name'          => __( 'Copyright', THEME_NAME ),
            'description'   => __( 'Adicionar widgets ao footer do tema.', THEME_NAME ),
            'before_widget' => '<section id="copyright">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
        );
        register_sidebar( $args );
    }

    public function init_enqueue_ajax_login() {
        global $data;

        wp_register_script('ajax-login-script', THEME_URI . '/js/ajax-login.js', array('jquery'), '1.0' );
        
        wp_enqueue_script('ajax-login-script');
        
        wp_localize_script( 'ajax-login-script', 'ajax_login_object', array( 
            'ajaxurl'        => admin_url( 'admin-ajax.php' ),
            'redirecturl'    => site_url('/wp-admin/index.php'),
            'loadingmessage' => __( 'Fazendo login, aguarde...', THEME_NAME )
        ));

        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

    }

    public function my_acf_google_map_api( $api ){    
        $api['key'] = 'AIzaSyDUDI1_0FsZAMZcJJH3WJ-laySAJO-iMZ4';        
        return $api;        
    }

    public function page_title( $echo = false ){           
            
        if( is_home() ){
            
            // Blog ---------------------------------------
            $title = wp_title() . ' - ' . get_bloginfo('name');
            
        } elseif( is_tag() ){
            
            // Blog | Tag ---------------------------------
            $title = single_tag_title('', false) . ' - ' . get_bloginfo('name');
            
        } elseif( is_category() ){
            
            // Blog | Category ----------------------------
            $title = single_cat_title('', false) . ' - ' . get_bloginfo('name');
            
        } elseif( is_author() ){
            
            // Blog | Author ------------------------------
            $title = get_the_author() . ' - ' . get_bloginfo('name');
        
        } elseif( is_day() ){
        
            // Blog | Day ---------------------------------
            $title = get_the_time('d') . ' - ' . get_bloginfo('name');
        
        } elseif( is_month() ){
        
            // Blog | Month -------------------------------
            $title = get_the_time('F') . ' - ' . get_bloginfo('name');

        } elseif( is_year() ){
        
            // Blog | Year --------------------------------
            $title = get_the_time('Y') . ' - ' . get_bloginfo('name');
            
        } elseif( is_single() || is_page() ){
            
            // Single -------------------------------------
            $title = get_the_title( get_the_ID() ) . ' - ' . get_bloginfo('name');
            
        } elseif( get_post_taxonomies() ){
            
            // Taxonomy -----------------------------------
            $title = single_cat_title('', false) . ' - ' . get_bloginfo('name');
            
        } else {
            
            // Default ------------------------------------
            $title = get_the_title( get_the_ID() ) . ' - ' . get_bloginfo('name');     
        }
        
        if( $echo ) echo $title;
        return $title;
    }

    public function add_excerpts_to_pages() {
         add_post_type_support( 'page', 'excerpt' );
    }

    public function remove_thumbnail_dimensions( $html ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }

    public function wp_get_attachment( $attachment_id ) {

        $attachment = get_post( $attachment_id );
        return array(
            'alt'         => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
            'caption'     => $attachment->post_excerpt,
            'description' => $attachment->post_content,
            'href'        => get_permalink( $attachment->ID ),
            'src'         => $attachment->guid,
            'title'       => $attachment->post_title
        );
    }

    public function init_pagination(){
        global $wp_query ;
        $big = 999999999;

        $pages = paginate_links(array(
            'base'      => str_replace($big, '%#%', get_pagenum_link($big)),
            'format'    => '?page=%#%',
            'current'   => max(1, get_query_var('paged')),
            'total'     => $wp_query->max_num_pages,
            'type'      => 'array',
            'prev_next' => true,
            'prev_next' => true,
            'prev_text' => '<i class="fa fa-angle-left"></i>',
            'next_text' => '<i class="fa fa-angle-right"></i>'
        ));
        if (is_array($pages)) {
            $current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
            echo '<ul class="pagination pagination-sm">';
            foreach ($pages as $i => $page) {
                if ($current_page == 1 && $i == 0) {
                    echo "<li class='active'>$page</li>";
                } else {
                    if ($current_page != 1 && $current_page == $i) {
                        echo "<li class='active'>$page</li>";
                    } else {
                        echo "<li class=''>$page</li>";
                    }
                }
            }
            echo '</ul>';
        }
    }

    public function init_ajax_login(){

        // First check the nonce, if it fails the function will break
        check_ajax_referer( 'ajax-login-nonce', 'security' );

        // Nonce is checked, get the POST data and sign user on
        $info = array();
        $info['user_login']    = $_POST['username'];
        $info['user_password'] = $_POST['password'];
        $info['remember']      = true;

        $user_signon = wp_signon( $info, false );

        if ( is_wp_error($user_signon) ){
            echo json_encode(
                array(
                    'loggedin' => false, 
                    'message'  => __( 'Nome de usuário ou senha incorretos.', THEME_NAME )
                )
            );
        } else {
            echo json_encode(
                array(
                    'loggedin' => true, 
                    'message'  => __( 'Login feito com sucesso, redirecionando...', THEME_NAME )
                )
            );             
        }

        die();
    }

        // Returns the social network name given a URL
    public function get_social_network_sherad_id($url) {
        if(strpos($url, "twitter.com") !== false){
            return "twitter";
        }else if(strpos($url, "facebook.com") !== false){
            return "facebook";
        }else if(strpos($url, "plus.google.com") !== false){
            return "google-plus";
        }else if(strpos($url, "pinterest.com") !== false){
            return "pinterest";
        }else if(strpos($url, "tumblr.com") !== false){
            return "tumblr";
        }else if(strpos($url, "linkedin.com") !== false){
            return "linkedin";
        }else if(strpos($url, "instagram.com") !== false){
            return "instagram";
        }else if(strpos($url, "skype.com") !== false){
            return "skype";
        }else{
            return "unknown";
        }

        return $url;
    }
    
    public function uri_path_script() { 
        echo '<script type="text/javascript">';        
            echo 'var js_uri = "'.get_stylesheet_directory_uri().'"';
        echo '</script>';
    }

}

new Drs;