<?php
/*
		Template Name: WhatsApp Redirect
*/

 get_header(); ?>
	
		<main id="single-<?php the_ID(); ?>" <?php post_class('interna'); ?>>
			<section id="interna-content" class="container-fludi interna--contain">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
								<h1 style="padding-top:50px;">Redirecionando...</h1>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

<script>
	setTimeout(function(){
	 window.location.href = 'https://api.whatsapp.com/send?phone=5511950476969';
         }, 2000);
</script>

<?php get_footer(); ?>