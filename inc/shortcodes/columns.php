<?php
	/// 1/3 COLUMN
	function one_third($atts, $content = null) {
		return '<div class="col-xs-12 col-sm-6 col-md-4">'.do_shortcode($content).'</div>';
	}
	add_shortcode ("one_third", "one_third");
	
	/// 2/3 COLUMN
	function two_third($atts, $content = null) {
		return '<div class="col-xs-12 col-sm-6 col-md-8">'.do_shortcode($content).'</div>';
	}
	add_shortcode ("two_third", "two_third");
	
	/// 1/4 COLUMN
	function one_quarter($atts, $content = null) {
		return '<div class="col-xs-12 col-sm-6 col-md-3">'.do_shortcode($content).'</div>';
	}
	add_shortcode ("one_quarter", "one_quarter");

	/// 3/4 COLUMN
	function three_quarter($atts, $content = null) {
		return '<div class="col-xs-12 col-sm-6 col-md-9">'.do_shortcode($content).'</div>';
	}
	add_shortcode ("three_quarter", "three_quarter"); 

	function grid_columns ( $atts ) {
	    $atts = extract( shortcode_atts( array(
			'amount'  => 12,
			'classes' => '',
	    ), $atts ) );

	    return '<div class="col-xs-12 col-sm-6 col-md-' . $atts['amount'] . ' ' . $atts['classes'] . '">' . do_shortcode($content). '</div>';
	}
	add_shortcode( 'columns', 'grid_columns' );
	//// [columns amount='12' classes='class-1 class-2']text[/columns]

?>