<?php 
function section_feed_shortcode( $atts ) {
	extract( shortcode_atts( array( 'limit' => -1, 'type' => 'post'), $atts ) );

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;  

	query_posts(  array ( 
		'posts_per_page' => $limit, 
		'post_type'      => $type, 
		'order'          => 'ASC', 
		'orderby'        =>'menu_order', 
		'paged'          => $paged ) );

	$list = ' ';   

	while ( have_posts() ) { 
		the_post();

	    $list .= '<article class="listing-view clearfix">' 
	    . '<div class="listing-content">' 
	    . '<h3><a href="' . get_permalink() . '">' . get_the_title() . '</a></h3>' 
	    .'<p>' . get_the_excerpt() . '</p>'
	    . '<a href="' . get_permalink() . '">' . 'View &raquo;' . '</a>'
	    . '</div>'
	    . '<a class="listing-thumb" href="' . get_permalink() . '">' . get_the_post_thumbnail($page->ID, 'listing-thumb')  . '<span></span></a>'
	    . '</article>';
	}

	return 
	'<div class="listings clearfix">' 
	. $list 
	. '<div class="nav-previous">' . get_next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts' ) ) . '</div>'
	. '<div class="nav-next">' . get_previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>' ) ) . '</div>'
	. '</div>' .
	
	wp_reset_query();

}
add_shortcode( 'feed', 'section_feed_shortcode' );

?>