<?php
class Widget_Copyright extends WP_Widget{
    /**
     * Constructor
     **/
    public function __construct(){
        $widget_ops = array(
            'classname'   => 'copyright',
            'description' => __('Adicionar imagens.')
        );

        parent::__construct( 'copyright', __('SM: Copyright'), $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
    }

    /**
     * Upload the Javascripts for the media uploader
     */
    public function upload_scripts() {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', get_template_directory_uri() . '/js/upload-media-widget.js', array('jquery'));

        wp_enqueue_style('thickbox');
    }

    /**
     * Outputs the HTML for this widget.
     *
     * @param array  An array of standard parameters for widgets in this theme
     * @param array  An array of settings for this widget instance
     * @return void Echoes it's output
     **/
    public function widget( $args, $instance ) {

        extract( $args );
        
        if($instance[ 'text-copy' ] || $instance[ 'imagem' ]) :
            $output = '<section class="container-fluid copy">';
                $output .= '<p>'.$instance[ 'text-copy' ].'</p>';
            
                if($instance[ 'imagem' ]) :
                    $output .= '<a href="'.$instance[ 'link' ].'" target="blank" class="click">';
                        $output .= '<img src="'.$instance[ 'imagem' ].'" alt="" />';
                    $output .= '</a>';
                endif;
                
            $output .= '</section>';

            echo $output;
        endif;
                   

    }

    /**
     * Deals with the settings when they are saved by the admin. Here is
     * where any validation should be dealt with.
     *
     * @param array  An array of new settings as submitted by the admin
     * @param array  An array of the previous settings
     * @return array The validated and (if necessary) amended settings
     **/
    public function update( $new_instance, $old_instance ) {
        // update logic goes here
        $updated_instance = $new_instance;
        return $updated_instance;
    }

    /**
     * Displays the form for this widget on the Widgets page of the WP Admin area.
     *
     * @param array  An array of the current settings for this widget
     * @return void
     **/
    public function form( $instance ) {
        $defaults = array(
            'text-copy' => '',
            'imagem'    => '',
            'link'      => ''
        );
        $instance = wp_parse_args( (array) $instance, $defaults );

        $image = '';
        if(isset($instance['imagem'])) {
            $image = $instance['imagem'];
        }
    ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'text-copy' ) ); ?>"><?php esc_html_e( 'Texto:', THEME_NAME ); ?></label> 
            <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text-copy' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text-copy' ) ); ?>" rows="4"><?php echo esc_html( $instance['text-copy'] ); ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_name( 'imagem' ); ?>" style="width: 100%; float:left;"><?php _e( 'Carregar imagem:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'imagem' ); ?>" id="<?php echo $this->get_field_id( 'imagem' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $image ); ?>" style="width: 70%; float:left;"/>
            <input class="upload_image_button button-primary" type="button" value="<?php echo __('Upload'); ?>" style="width: 27%; float:left; margin-left:3%;" />
            <?php
                if ( $image ) :
                    echo '<img class="custom_media_image" src="' . $image . '" style="margin:5px 0 0 0;border:4px solid #E4DEDE;padding:0;max-width:100%;width: 100%;float:left;display:inline-block;clear:both;box-sizing:border-box;" />';
                endif;
            ?>
        </p>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>"><?php esc_html_e( 'Link:', THEME_NAME ); ?></label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'link' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['link'] ); ?>" />
        </p>
    <?php
    }
}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Copyright");' ) );