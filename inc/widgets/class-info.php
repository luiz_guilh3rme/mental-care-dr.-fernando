<?php

class Widget_Info extends WP_Widget {

	function __construct() {
		parent::__construct(
			'widget_infos',
			__( 'SM: Informações', THEME_NAME ),
			array(
				'classname'                   => 'widget_infos',
				'description'                 => esc_html__( 'Adicionar informações sobre o site.', THEME_NAME ),
				'customize_selective_refresh' => true
			)
		);

		add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
	}

	/**
     * Upload the Javascripts for the media uploader
     */
    public function upload_scripts() {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', get_template_directory_uri() . '/js/upload-media-widget.js', array('jquery'));

        wp_enqueue_style('thickbox');
    }

	public function widget( $args, $instance ) {

		if ( ! isset( $args['widget_id'] ) ) $args['widget_id'] = null;
		extract( $args, EXTR_SKIP );

-		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base);

		echo $before_widget;
		
		ob_start();

		if ( ! empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}
		  
	?>
			
		<?php if($instance['imagem']) { echo '<img src="'.$instance['imagem'].'" />'; } ?>

		<?php if($instance['description']) { echo '<p>'.$instance['description'].'</p>'; } ?>

		<div class="list-item">
			<ul>
				<?php if($instance['tel']) : ?>
					<li class="item-phone">
						<i class="fa fa-phone"></i>
						<span><?php echo $instance['tel']; ?></span>
					</li>
				<?php endif; ?>

				<?php if($instance['mail']) : ?>
					<li class="item-mail">
						<i class="icon-mail"></i>
						<span><?php echo $instance['mail']; ?></span>												
					</li>
				<?php endif; ?>

				<?php if($instance['address']) : ?>
					<li class="item-address">
						<i class="fa fa-map-marker"></i>
						<span><?php echo $instance['address']; ?></span>
					</li>
				<?php endif; ?>

			</ul>
		</div>
		<hr>
		<div class="footer-social">
            <ul>
            	
            	<?php if($instance['perfil-1']) : ?>
	                <li class="social-<?php echo get_social_network_sherad_id($instance['perfil-1']); ?>">
	                	<a href="<?php echo $instance['perfil-1']; ?>" target="_blank" class="<?php echo get_social_network_sherad_id($instance['perfil-1']); ?>">
	                		<i class="fa fa-<?php echo get_social_network_sherad_id($instance['perfil-1']); ?>-square" aria-hidden="true"></i>
	                	</a>
	                </li>
                <?php endif; ?>

                <?php if($instance['perfil-2']) : ?>
	                <li class="social-<?php echo get_social_network_sherad_id($instance['perfil-2']); ?>">
	                	<a href="<?php echo $instance['perfil-2']; ?>" target="_blank" class="<?php echo get_social_network_sherad_id($instance['perfil-2']); ?>">
	                		<i class="fa fa-<?php echo get_social_network_sherad_id($instance['perfil-2']); ?>" aria-hidden="true"></i>
	                	</a>
	                </li>
                <?php endif; ?>

            </ul>                    
        </div>

	<?php

		$content = apply_filters( 'categories_widget', ob_get_clean(), $instance, $args );

		echo $content;

		echo $after_widget;

	}

	public function form( $instance ) {

		$defaults = array(
			'title'       => __( 'Informações', THEME_NAME ),
			'imagem'      => '',
			'description' => '',
			'tel'         => '',
			'mail'        => '',
			'address'     => '',
			'perfil-1'    => '',
			'perfil-2'    => ''
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Título:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
            <label for="<?php echo $this->get_field_id( 'imagem' ); ?>" style="width: 100%; float:left;"><?php esc_html_e( 'Browser:', THEME_NAME ); ?></label>
            <input name="<?php echo $this->get_field_name( 'imagem' ); ?>" id="<?php echo $this->get_field_id( 'imagem' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $instance['imagem'] ); ?>" style="width: 70%; float:left;"/>
            <input class="upload_image_button button-primary" type="button" value="<?php esc_html_e( 'Upload:', THEME_NAME ); ?>" style="width: 27%; float:left; margin-left:3%;" />
            <?php
                if ( $instance['imagem'] ) :
                    echo '<img class="custom_media_image" src="' . $instance['imagem'] . '" style="margin:5px 0 0 0;border:4px solid #E4DEDE;padding:0;max-width:100%;width: 100%;float:left;display:inline-block;clear:both;box-sizing:border-box;" />';
                endif;
            ?>
        </p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_html_e( 'Descrição:', THEME_NAME ); ?></label> 
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" rows="4"><?php echo esc_html( $instance['description'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'tel' ) ); ?>"><?php esc_html_e( 'Telefone:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'tel' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'tel' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['tel'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'mail' ) ); ?>"><?php esc_html_e( 'E-mail:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'mail' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'mail' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['mail'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>"><?php esc_html_e( 'Endereço:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'address' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['address'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'perfil-1' ) ); ?>"><?php esc_html_e( 'URL Perfil Solcial:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'perfil-1' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'perfil-1' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['perfil-1'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'perfil-2' ) ); ?>"><?php esc_html_e( 'URL Perfil Solcial:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'perfil-2' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'perfil-2' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['perfil-2'] ); ?>" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title']       = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['imagem']      = ( ! empty( $new_instance['imagem'] ) ) ? strip_tags( $new_instance['imagem'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		$instance['tel']         = ( ! empty( $new_instance['tel'] ) ) ? strip_tags( $new_instance['tel'] ) : '';
		$instance['mail']        = ( ! empty( $new_instance['mail'] ) ) ? strip_tags( $new_instance['mail'] ) : '';
		$instance['address']     = ( ! empty( $new_instance['address'] ) ) ? strip_tags( $new_instance['address'] ) : '';
		$instance['perfil-1']     = ( ! empty( $new_instance['perfil-1'] ) ) ? strip_tags( $new_instance['perfil-1'] ) : '';
		$instance['perfil-2']     = ( ! empty( $new_instance['perfil-2'] ) ) ? strip_tags( $new_instance['perfil-2'] ) : '';

		return $instance;
	}

	// Returns the social network name given a URL
	public function get_social_network_sherad_id($url) {
		if(strpos($url, "github.com") !== false){
			return "github";
		}else if(strpos($url, "flickr.com") !== false){
			return "flickr";
		}else if(strpos($url, "twitter.com") !== false){
			return "twitter";
		}else if(strpos($url, "facebook.com") !== false){
			return "facebook";
		}else if(strpos($url, "plus.google.com") !== false){
			return "googleplus";
		}else if(strpos($url, "pinterest.com") !== false){
			return "pinterest";
		}else if(strpos($url, "tumblr.com") !== false){
			return "tumblr";
		}else if(strpos($url, "linkedin.com") !== false){
			return "linkedin";
		}else if(strpos($url, "dribbble.com") !== false){
			return "dribbble";
		}else if(strpos($url, "stumbleupon.com") !== false){
			return "stumbleupon";
		}else if(strpos($url, "last.fm") !== false){
			return "lastfm";
		}else if(strpos($url, "rdio.com") !== false){
			return "rdio";
		}else if(strpos($url, "spotify.com") !== false){
			return "spotify";
		}else if(strpos($url, "qq.com") !== false){
			return "qq";
		}else if(strpos($url, "instagram.com") !== false){
			return "instagram";
		}else if(strpos($url, "dropbox.com") !== false){
			return "dropbox";
		}else if(strpos($url, "evernote.com") !== false){
			return "evernote";
		}else if(strpos($url, "flattr.com") !== false){
			return "flattr";
		}else if(strpos($url, "skype.com") !== false){
			return "skype";
		}else if(strpos($url, "renren.com") !== false){
			return "renren";
		}else if(strpos($url, "weibo.com") !== false){
			return "sina-weibo";
		}else if(strpos($url, "paypal.com") !== false){
			return "paypal";
		}else if(strpos($url, "picasa.google.com") !== false){
			return "picasa";
		}else if(strpos($url, "soundcloud.com") !== false){
			return "soundcloud";
		}else if(strpos($url, "mixi.co.jp") !== false){
			return "mixi";
		}else if(strpos($url, "behance.net") !== false){
			return "behance";
		}else if(strpos($url, "/circles/") !== false){
			return "google-circles";
		}else if(strpos($url, "vk.com") !== false){
			return "vk";
		}else if(strpos($url, "smashingmagazine.com") !== false){
			return "smashing";
		}else{
			return "unknown";
		}

		return $url;
	}

}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Info");' ) );