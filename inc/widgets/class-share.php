<?php
class Widget_Share extends Custom_Widget {
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'widget_share';
		$this->widget_description = __( 'Mostrar botões de compartilhamento em redes sociais' );
		$this->widget_id          = 'widget_share';
		$this->widget_name        = __( 'SM: Botões para Compartilhar' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Title:' )
			),
			'show_facebook' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Mostar Facebook' )
			),
			'show_twitter' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Mostar Twitter' )
			),
			'show_pinterest' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Mostar Pinterest' )
			),
			'show_gplus' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Mostar Google+' )
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		global $post;

		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : '', $instance, $this->id_base );

		$facebook         = $instance[ 'show_facebook' ];
		$twitter          = $instance[ 'show_twitter' ];
		$pinterest        = $instance[ 'show_pinterest' ];
		$googleplus       = $instance[ 'show_gplus' ];

		// Get the post ID
		$post_id      = get_the_ID(); 
		$post_title   = urlencode(get_the_title());
		$post_url     = urlencode(get_permalink());
		$post_content = urlencode(get_the_content());
		$post_image   = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),  'full' );

		echo $before_widget;

			if ( $title ) echo $before_title . $title . $after_title;

			if($facebook == 1){
				$facebook = true;
			}
			if($twitter == 1){
				$twitter = true;
			}
			if($pinterest == 1){
				$pinterest = true;
			}
			if($googleplus == 1){
				$googleplus = true;
			}
			
			$temp_before = '<div class="social-icons-wrap"><div class="don-share" data-style="icons" data-bubbles="right" data-limit="6">';
				if($facebook):
					$tmp_face = '<div class="don-share-facebook">';
						$tmp_face .= '<a href="https://www.facebook.com/sharer/sharer.php?s=100&amp;p[url]='.$post_url.'" target="blank"></a>';
					$tmp_face .= '</div>';
				endif;

				if($twitter):
					$temp_twi = '<div class="don-share-twitter">';
						$temp_twi .= '<a href="https://twitter.com/share?&amp;url='.$post_url.'&amp;text='.$post_title.'" target="blank"></a>';
					$temp_twi .= '</div>';
				endif;

				if($googleplus):
					$tmp_gplus = '<div class="don-share-google">';
						$tmp_gplus .= '<a href="https://plus.google.com/share?url='.$post_url.'" target="blank"></a>';
					$tmp_gplus .= '</div>';
				endif;

				if($pinterest):
					$tmp_pin = '<div class="don-share-pinterest">';
						$tmp_pin .= '<a href="http://pinterest.com/pin/create/button/?url='.$post_url.'&amp;media='.$post_image[0].'" target="blank"></a>';
					$tmp_pin .= '</div>';
				endif;
			$temp_after = '</div></div>';

			echo $temp_before.$tmp_face.$temp_twi.$tmp_pin.$tmp_gplus.$temp_after.'<script type="text/javascript">(function(){var dr=document.createElement("script");dr.type="text/javascript";dr.async=true;dr.src="//share.donreach.com/buttons.js";(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(dr);})();</script>';
			

		echo $after_widget;

		$content = apply_filters( 'widget_share', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Share");' ) );