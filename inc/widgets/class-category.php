<?php
/**
 * List Blog Recents Posts
 *
 * @since Bazar do Tempo 1.0
 */
class Widget_Categorias extends Custom_Widget {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'categories_widget';
		$this->widget_description = __( 'Mostrar todas as categrias.', THEME_NAME );
		$this->widget_id          = 'categories_widget';
		$this->widget_name        = __( 'SM: Mostrar categorias', THEME_NAME );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => __( 'Categorias', THEME_NAME ),
				'label' => __( 'Título:', THEME_NAME )
			),
			'show_count' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Mostar Quantidade' )
			),
		);
		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	public function widget( $args, $instance ) {
		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );

		$args = array(
			'hide_empty' => 0
		);
	 
		// retrieves an array of categories or taxonomy terms
		$cats = get_categories($args);

		echo $before_widget;

		ob_start(); 
	?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		
		<ul class="sidebar--menu">
			<?php foreach($cats as $cat) { ?>
				<li class="sidebar--menu-item">
					<?php if(isset($show_count) == 0) : ?>
						<a href="<?php echo get_category_link($cat->term_id); ?>" class="link" title="Ver todos os posts da categoria <?php echo $cat->name; ?>">
							<?php echo $cat->name; ?>
						</a>
					<?php else: ?>
						<a href="<?php echo get_category_link($cat->term_id); ?>" title="Ver todos os posts da categoria <?php echo $cat->name; ?>" class="link">
							<?php echo $cat->name; ?>&nbsp;(<?php echo $cat->category_count; ?>)
						</a>
					<?php endif; ?>
				</li>
			<?php } ?>
		</ul>
	<?php

		$content = apply_filters( 'categories_widget', ob_get_clean(), $instance, $args );

		echo $content;

		echo $after_widget;

		$this->cache_widget( $args, $content );
	}
}
register_widget( 'Widget_Categorias' );