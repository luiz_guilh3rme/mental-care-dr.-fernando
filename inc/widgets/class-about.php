<?php

class Widget_About extends WP_Widget {

	function __construct() {
		
		parent::__construct(
			'about_widget',
			esc_html__( 'SM: Autor/Atura' ),
			array( 'description' => esc_html__( 'Sobre o autor ou autora.' ), )
		);

		add_action( 'admin_enqueue_scripts', array( $this, 'upload_scripts' ) );
	}

	public function upload_scripts() {

    wp_enqueue_media();
    wp_enqueue_script( 'widget_upload', get_template_directory_uri() . '/js/upload-media-widget.js', array( 'jquery' ) );
  }

	public function widget( $args, $instance ) {

		extract( $args );
		$title             = apply_filters( 'widget_title', $instance['title'] );
		$profile_image     = $instance['profile_image'];
		$description       = $instance['description'];
		$profile_facebook  = $instance['profile_facebook'];
		$profile_instagram = $instance['profile_instagram'];
		$link_readmore     = $instance['link_readmore'];

		if ( ! empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}

		echo $before_widget;

		ob_start(); 
	?>

	
		<?php if ( $profile_image != '' ) : ?>
			<img src="<?php echo esc_url( $profile_image ); ?>">
		<?php endif; ?>

		<?php if ( $description != '' ) : ?>
			<p>
				<?php echo wp_kses( $description, array(
					'a'      => array( 'href' => array() ),
					'span'   => array( 'style' => array() ),
					'i'      => array( 'class' => array(), 'style' => array() ),
					'em'     => array(),
					'strong' => array()
				) ); ?>
			</p>
		<?php endif; ?>

		<div class="links">
			<ul class="socials">
			<?php if ( $profile_instagram != '' ) : ?>
				<li>
					<a href="<?php echo $profile_instagram; ?>" targe="_blank"><i class="fa fa-instagram"></i></a>
				</li>
			<?php endif; ?>
			<?php if ( $profile_facebook != '' ) : ?>
				<li>
					<a href="<?php echo $profile_facebook; ?>" targe="_blank"><i class="fa fa-facebook"></i></a>
				</li>
			<?php endif; ?>
			</ul>
			<?php if ( $link_readmore != '' ) : ?>
				<a href="<?php echo get_permalink($link_readmore); ?>"><?php echo __('Leia mais'); ?></a>
			<?php endif; ?>
		</div>

		<?php

		echo ob_get_clean();

		echo $after_widget;
	}

	public function form( $instance ) {

		$defaults = array(
			'title'             => '',
			'profile_image'     => '',
			'description'       => '',
			'profile_facebook'  => '',
			'profile_instagram' => '',
			'link_readmore'     => '',
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

	?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Título:' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_name( 'profile_image' ) ); ?>" style="width: 100%; float:left;"><?php esc_html_e( 'Foto do Perfil:' ); ?></label>
			<input class="widefat image-url" id="<?php echo esc_attr( $this->get_field_id( 'profile_image' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'profile_image' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['profile_image'] ); ?>" style="width: 70%; float:left;" />
			<input class="upload-button button-primary" type="button" value="<?php esc_html_e( 'Upload' ); ?>" style="width: 27%; float:left; margin-left:3%;" />
			<?php
	            if ( $instance['profile_image'] ) :
	                echo '<img class="custom_media_image" src="' . $instance['profile_image'] . '" style="margin:5px 0 0 0;border:4px solid #E4DEDE;padding:0;max-width:100%;width: 100%;float:left;display:inline-block;clear:both;box-sizing:border-box;" />';
	            endif;
            ?>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_html_e( 'Descrição:' ); ?></label> 
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" rows="4"><?php echo esc_html( $instance['description'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'profile_facebook' ) ); ?>"><?php esc_html_e( 'Perfil Facebook:' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'profile_facebook' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'profile_facebook' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['profile_facebook'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'profile_instagram' ) ); ?>"><?php esc_html_e( 'Perfil Instagram:' ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'profile_instagram' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'profile_instagram' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['profile_instagram'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'link_readmore' ); ?>"><?php esc_html_e( 'Página do Perfil:' ); ?></label>
			<?php 
				$post_type_object = get_post_type_object('page');
		        $label = $post_type_object->label;
		        $posts = get_posts(
		        	array(
						'post_type'        => 'page', 
						'post_status'      => 'publish', 
						'suppress_filters' => false, 
						'posts_per_page'   =>-1
		        	)
		        );
			?>
			<select class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'link_readmore' ) ); ?>" name="<?php echo $this->get_field_name('link_readmore' ); ?>">
				<option value="" ><?php esc_html_e( 'Escolha uma Páginas' ); ?></option>
				<?php foreach ($posts as $post) { ?>
					<option value="<?php echo $post->ID; ?>" <?php selected( $post->ID, $instance['link_readmore'] ); ?> ><?php echo $post->post_title; ?></option>
				<?php } ?>
			</select>
		</p>
	<?php 
	}

	public function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title']             = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['profile_image']     = ( ! empty( $new_instance['profile_image'] ) ) ? strip_tags( $new_instance['profile_image'] ) : '';
		$instance['description']       = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		$instance['profile_facebook']  = ( ! empty( $new_instance['profile_facebook'] ) ) ? strip_tags( $new_instance['profile_facebook'] ) : '';		
		$instance['profile_instagram'] = ( ! empty( $new_instance['profile_instagram'] ) ) ? strip_tags( $new_instance['profile_instagram'] ) : '';	
		$instance['link_readmore']     = ( ! empty( $new_instance['link_readmore'] ) ) ? strip_tags( $new_instance['link_readmore'] ) : '';

		return $instance;
	}

}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_About");' ) );