<?php
class Widget_Count_Post_Views extends WP_Widget {

	/* ---------------------------------------------------------------------------
	 * Constructor
	 * --------------------------------------------------------------------------- */
	function __construct(){
		
		$widget_ops = array( 'classname' => 'widget_count_post_views', 'description' => __( 'Lista as postagens mais lidas do blog.', THEME_NAME ) );
		
		parent::__construct( 'widget_count_post_views', __( 'SM: Posts mais lidos', THEME_NAME ), $widget_ops );
		
		$this->alt_option_name = 'widget_count_post_views';

		// Executa a ação
    	add_action( 'init', array($this, 'tutsup_session_start') );

    	add_action( 'get_header',  array($this, 'tp_count_post_views') );
	}

	function tutsup_session_start() {
        // Inicia uma sessão PHP
        if ( ! session_id() ) session_start();
    }

    // Conta os views do post
    function tp_count_post_views () {	
        // Garante que vamos tratar apenas de posts
        if ( is_single() ) {
        
            // Precisamos da variável $post global para obter o ID do post
            global $post;
            
            // Se a sessão daquele posts não estiver vazia
            if ( empty( $_SESSION[ 'tp_post_counter_' . $post->ID ] ) ) {
                
                // Cria a sessão do posts
                $_SESSION[ 'tp_post_counter_' . $post->ID ] = true;
            
                // Cria ou obtém o valor da chave para contarmos
                $key = 'tp_post_counter';
                $key_value = get_post_meta( $post->ID, $key, true );
                
                // Se a chave estiver vazia, valor será 1
                if ( empty( $key_value ) ) { // Verifica o valor
                    $key_value = 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } else {
                    // Caso contrário, o valor atual + 1
                    $key_value += 1;
                    update_post_meta( $post->ID, $key, $key_value );
                } // Verifica o valor
                
            } // Checa a sessão
            
        } // is_single
        
        return;
        
    }    

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	/* ---------------------------------------------------------------------------
	 * Outputs the HTML for this widget.
	 * --------------------------------------------------------------------------- */
	function widget( $args, $instance ) {

		if ( ! isset( $args['widget_id'] ) ) $args['widget_id'] = null;
		extract( $args, EXTR_SKIP );

		global $post;

		$args = array(
	        'posts_per_page'      => $instance['count'] ? intval($instance['count']) : 0, // Máximo de 5 artigos
			'showposts'           => $instance['count'] ? intval($instance['count']) : 0, // Máximo de 5 artigos
	        'no_found_rows'       => true,              // Não conta linhas
	        'post_status'         => 'publish',         // Somente posts publicados
	        'ignore_sticky_posts' => true,              // Ignora posts fixos
	        'orderby'             => 'meta_value_num',  // Ordena pelo valor da post meta
	        'meta_key'            => 'tp_post_counter', // A nossa post meta
	        'order'               => 'DESC',            // Ordem decrescente
	        'category__not_in'    => array($instance['exclude_cat'] ? intval($instance['exclude_cat']) : 0),
		);
		$r = new WP_Query( apply_filters( 'widget_posts_args', $args ) );

		echo $before_widget;

		ob_start();
		
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base);
		if( $title ) echo $before_title . $title . $after_title;

		if ($r->have_posts()){   
	?>        
			<div class="bloco">
				<ul class="list">
					<?php while ( $r->have_posts() ){ $r->the_post(); ?>							
						<li>
							<a href="<?php echo get_permalink(); ?>" class="text"><?php echo get_the_title(); ?></a>
                       	</li>                       	
					<?php } wp_reset_postdata(); ?>
				</ul>
			</div>
	<?php
		}

		echo ob_get_clean();

		echo $after_widget;
	}

	/* ---------------------------------------------------------------------------
	 * Deals with the settings when they are saved by the admin.
	 * --------------------------------------------------------------------------- */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title']       = strip_tags( $new_instance['title'] );
		$instance['count']       = (int) $new_instance['count'];
		$instance['exclude_cat'] = (int) $new_instance['exclude_cat'];
		
		return $instance;
	}

	
	/* ---------------------------------------------------------------------------
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 * --------------------------------------------------------------------------- */
	function form( $instance ) {
		
		$title		= isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$count		= isset( $instance['count'] ) ? absint( $instance['count'] ) : 5;
		$exclude_cat		= isset( $instance['exclude_cat'] ) ? absint( $instance['exclude_cat'] ) : 0;

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', THEME_NAME ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"><?php _e( 'Number of posts:', THEME_NAME ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>" type="text" value="<?php echo esc_attr( $count ); ?>" size="3"/>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'exclude_cat' ) ); ?>"><?php _e( 'Categoria a ser excluída:', THEME_NAME ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'exclude_cat' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'exclude_cat' ) ); ?>" type="text" value="<?php echo esc_attr( $exclude_cat ); ?>" size="3"/>
			</p>
			
		<?php
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Count_Post_Views");' ) );