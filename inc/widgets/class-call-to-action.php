<?php
class Widget_Call_To_Action extends Custom_Widget {
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'widget_call_to_action';
		$this->widget_description = __( 'Adicionar call to action acima do footer', THEME_NAME );
		$this->widget_id          = 'widget_call_to_action';
		$this->widget_name        = __( 'SM: Call to action', THEME_NAME );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Título', THEME_NAME )
			),
			'texto' => array(
				'type'  => 'textarea',
				'std'   => '',
				'rows'  => 5,
				'label' => __( 'Adicionar texto', THEME_NAME )
			),
			'page_id' => array(
				'type'    => 'select',
				'label'   => __( 'Adicionar Link', THEME_NAME ),
				'std'     => '',
				'options' => $this->get_pages_id()
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		global $post;

		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title   = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : '', $instance, $this->id_base );
		$texto = $instance[ 'texto' ];
		$page_id = $instance[ 'page_id' ];

		echo $before_widget;

			$html = '<div class="container">';
				if ( $title ) $html .= $before_title . $title . $after_title;
				if(!empty($texto)){
					$html .= '<p>'.$texto.'</p>';
				}
				if($page_id){
					$html .= '<a href="'.esc_url(get_permalink($page_id)).'">';
						$html .= get_the_title($page_id);
						$html .= '<i class="fa fa-angle-right"></i>';
					$html .= '</a>';
				}
			$html .= '</div>';

			echo $html;

		echo $after_widget;

		$content = apply_filters( 'widget_call_to_action', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}

	function get_pages_id() {        
		$url  = new WP_Query( array(
			'post_type'      => array( 'page' ),
			'posts_per_page' => -1
		) );

		if ( ! $url->have_posts() )
			return array();

		$_pages = array_combine(
			wp_list_pluck( $url->posts, 'ID' ),
			wp_list_pluck( $url->posts, 'post_title' )
		);

		return $_pages;		
    }
}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Call_To_Action");' ) );