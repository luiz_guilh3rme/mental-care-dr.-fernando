<?php

class Widget_Recents_Posts extends WP_Widget {
	
	/* ---------------------------------------------------------------------------
	 * Constructor
	 * --------------------------------------------------------------------------- */
	function __construct() {
		parent::__construct(
			'widget_recent_posts',
			__( 'SM: Posts mais recentes', THEME_NAME ),
			array(
				'classname'                   => 'widget_recent_posts',
				'description'                 => esc_html__( 'Mostra as publicações mais recentes do blog.', THEME_NAME ),
				'customize_selective_refresh' => true
			)
		);
	}
	
	
	/* ---------------------------------------------------------------------------
	 * Outputs the HTML for this widget.widget_lastnewswidget_lastnews
	 * --------------------------------------------------------------------------- */
	function widget( $args, $instance ) {

		if ( ! isset( $args['widget_id'] ) ) $args['widget_id'] = null;
		extract( $args, EXTR_SKIP );

		echo $before_widget;
		
		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base);

		$parentID = 11;
				
		$args = array(
			'posts_per_page'		=> $instance['count'],
			'no_found_rows'			=> true,
			'post_status'			=> 'publish',
			'ignore_sticky_posts'	=> true,
			'category__not_in'      => array($instance['excludeCat']),
		);
		
		$r = new WP_Query( apply_filters( 'widget_posts_args', $args ) );
		
		$output = false;
		if ($r->have_posts()){           

			if( $title ) echo $before_title . $title . $after_title;

			$output .= '<ul class="sidebar--thumbs">';
				while ( $r->have_posts() ){
					$r->the_post();
											
					$output .= '<li id="post-'.get_the_ID().'">';
						$output .= '<a href="'.get_permalink().'">';
							if( has_post_thumbnail() ) :
								$output .= '<div class="sidebar--thumbs-image">';
									$output .= get_the_post_thumbnail( get_the_ID(), 'widget-blog-post', array( 'class' => 'scale-with-grid' ) );
								$output .= '</div>';
							endif;
							$output .= '<div class="sidebar--thumbs-text">';								
								$output .= '<p>'.wp_trim_words( get_the_excerpt(), 8 ).'</p>';
							$output .= '</div>';
						$output .= '</a>';
					$output .= '</li>';                   	
				}
				wp_reset_postdata();
			$output .= '</ul>'."\n";
		}
		echo $output;

		echo $after_widget;
	}


	/* ---------------------------------------------------------------------------
	 * Deals with the settings when they are saved by the admin.
	 * --------------------------------------------------------------------------- */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		
		$instance['title']        = strip_tags( $new_instance['title'] );
		$instance['count']        = (int) $new_instance['count'];
		$instance['excludeCat']   = (int) $new_instance['excludeCat'];
		
		return $instance;
	}

	
	/* ---------------------------------------------------------------------------
	 * Displays the form for this widget on the Widgets page of the WP Admin area.
	 * --------------------------------------------------------------------------- */
	function form( $instance ) {
		
		$title        = isset( $instance['title']) ? esc_attr( $instance['title'] ) : '';
		$count        = isset( $instance['count'] ) ? absint( $instance['count'] ) : 5;
		$categoryName = isset( $instance['categoryName']) ? esc_attr( $instance['categoryName'] ) : '';
		$excludeCat   = isset( $instance['excludeCat'] ) ? absint( $instance['excludeCat'] ) : 0;

		?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Título:', THEME_NAME ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
						
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>"><?php _e( 'Número de Posts:', THEME_NAME ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'count' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'count' ) ); ?>" type="text" value="<?php echo esc_attr( $count ); ?>" size="3"/>
			</p>

			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'excludeCat' ) ); ?>"><?php _e( 'Excluir Categoria da lista:', THEME_NAME ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'excludeCat' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'excludeCat' ) ); ?>" type="text" value="<?php echo esc_attr( $excludeCat ); ?>" size="3"/>
			</p>
			
		<?php
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Recents_Posts");' ) );