<?php

class Widget_Newsletter extends WP_Widget {

	function __construct() {
		parent::__construct(
			'widget_newsletter',
			__( 'SM: Newsletter', THEME_NAME ),
			array(
				'classname'                   => 'widget_newsletter',
				'description'                 => esc_html__( 'Adicionar formúlário de newsletter.', THEME_NAME ),
				'customize_selective_refresh' => true
			)
		);
	}

	public function widget( $args, $instance ) {

		if ( ! isset( $args['widget_id'] ) ) $args['widget_id'] = null;
		extract( $args, EXTR_SKIP );

-		$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base);

		echo $before_widget;
		
		ob_start();

		if ( ! empty( $title ) ) {
			echo $before_title . $title . $after_title;
		}
		  
	?>
			
		<?php if($instance['description']) echo '<p>'.$instance['description'].'</p>'; ?>

		<?php if($instance['form']) : ?>
		<div class="formDefault">

			<?php echo do_shortcode( $instance['form'] ); ?>

		</div>
		<?php endif; ?>

		<?php if($instance['note']) echo '<em>'.$instance['note'].'</em>'; ?>
	<?php

		$content = apply_filters( 'categories_widget', ob_get_clean(), $instance, $args );

		echo $content;

		echo $after_widget;

	}

	public function form( $instance ) {

		$defaults = array(
			'title'           => __( 'Newsletter', THEME_NAME ),
			'description'     => '',
			'note'            => '',
			'form'            => ''
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Título:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>"><?php esc_html_e( 'Descrição:', THEME_NAME ); ?></label> 
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" rows="4"><?php echo esc_html( $instance['description'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'note' ) ); ?>"><?php esc_html_e( 'Nota:', THEME_NAME ); ?></label> 
			<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'note' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'note' ) ); ?>" rows="4"><?php echo esc_html( $instance['note'] ); ?></textarea>
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'form' ) ); ?>"><?php esc_html_e( 'Shortcode:', THEME_NAME ); ?></label> 
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'form' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'form' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['form'] ); ?>" />
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title']       = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? strip_tags( $new_instance['description'] ) : '';
		$instance['note'] = ( ! empty( $new_instance['note'] ) ) ? strip_tags( $new_instance['note'] ) : '';
		$instance['form'] = ( ! empty( $new_instance['form'] ) ) ? strip_tags( $new_instance['form'] ) : '';

		return $instance;
	}

}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Newsletter");' ) );