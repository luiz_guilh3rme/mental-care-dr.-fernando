<?php
class Widget_Social_Profile extends Custom_Widget {
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->widget_cssclass    = 'widget_social_profile';
		$this->widget_description = __( 'Adicionar perfis sociais', THEME_NAME );
		$this->widget_id          = 'widget_social_profile';
		$this->widget_name        = __( 'SM: Perfis sociais', THEME_NAME );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => '',
				'label' => __( 'Título', THEME_NAME )
			),
			'perfis' => array(
				'type'  => 'textarea',
				'std'   => '',
				'rows'  => 5,
				'label' => __( 'Adicionar os links dos perfis', THEME_NAME )
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 * @access public
	 * @param array $args
	 * @param array $instance
	 * @return void
	 */
	function widget( $args, $instance ) {
		global $post;

		if ( $this->get_cached_widget( $args ) )
			return;

		ob_start();

		extract( $args );

		$title   = apply_filters( 'widget_title', isset ( $instance[ 'title' ] ) ? $instance[ 'title' ] : '', $instance, $this->id_base );
		$perfis = $instance[ 'perfis' ];

		echo $before_widget;

			if ( $title ) echo $before_title . $title . $after_title;

			$sociais = "";
	        if(isset($perfis) && strlen($perfis) > 0){
	            $perfis_array = explode("\n", $perfis);
	            $sociais .= '<ul class="footer--socials">';
	            foreach($perfis_array as $perfil) {
	                $sociais .= '
	                    <li class="footer--socials-item">
	                        <a href="'.$perfil.'" target="_blank title="'. $this->pegar_perfis_sociais_id($perfil).'" id="'.$this->pegar_perfis_sociais_id($perfil).'" class="footer--socials-link">
	                            <i class="fa fa-'.$this->pegar_perfis_sociais_id($perfil).'"></i>
	                        </a>
	                    </li>
	                ';
	            }
	            $sociais .= '</ul>';
	        }
	        echo $sociais; 

		echo $after_widget;

		$content = apply_filters( 'widget_social_profile', ob_get_clean(), $instance, $args );

		echo $content;

		$this->cache_widget( $args, $content );
	}

	function pegar_perfis_sociais_id($url) {
        if(strpos($url, "twitter.com") !== false){
            return "twitter";
        }else if(strpos($url, "facebook.com") !== false){
            return "facebook";
        }else if(strpos($url, "plus.google.com") !== false){
            return "google-plus";
        }else if(strpos($url, "pinterest.com") !== false){
            return "pinterest";
        }else if(strpos($url, "tumblr.com") !== false){
            return "tumblr";
        }else if(strpos($url, "linkedin.com") !== false){
            return "linkedin";
        }else if(strpos($url, "instagram.com") !== false){
            return "instagram";
        }else if(strpos($url, "skype.com") !== false){
            return "skype";
        }else{
            return "unknown";
        }

        return $url;
    }
}
add_action( 'widgets_init', create_function( '', 'register_widget("Widget_Social_Profile");' ) );