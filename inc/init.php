<?php 
	DEFINE( 'LIBS_URI', get_template_directory() . '/inc/' );

	DEFINE( 'LIBS_DIR',        LIBS_URI . 'functions' );
	DEFINE( 'LIBS_POSTS',      LIBS_URI . 'post-types' );
	DEFINE( 'LIBS_WIDGET',     LIBS_URI . 'widgets' );
	DEFINE( 'LIBS_SHORTCODES', LIBS_URI . 'shortcodes' );
	DEFINE( 'LIBS_TAXONOMY',   LIBS_URI . 'taxonomies' );
	
	/* incluir libs php */
	$functions = array(
		'super-dump.php',
		'class-widget.php'
	);
	foreach ( $functions as $key => $dep ) {
		require_once( LIBS_DIR . '/' . $dep );
	}

	/* incluir custom post type no template */
	$post_types = array(
		'doencas',
		'perguntas',
		'tratamentos'
		
	);
	foreach ( $post_types as $key => $type  ) {
		require_once( LIBS_POSTS . '/' . $type . '.php' );
	}

	/* incluir novos widgets no template */
	$widgets = array(
		'class-about',
		'class-category',
		'class-facebook',
		'class-instagram',
		'class-share',
		'class-upload',
		'class-recent-posts',
		'class-count-post-views',
		'class-newsletter',
		'class-info',
		'class-social-profile',
		'class-copyright',
		'class-call-to-action'
	);
	foreach ( $widgets as $key => $widget  ) {
		require_once( LIBS_WIDGET . '/' . $widget . '.php' );
	}

	/* incluir novos shortcodes no template */
	$shortcodes = array(
		'columns'
	);
	foreach ( $shortcodes as $key => $shortcode  ) {
		require_once( LIBS_SHORTCODES . '/' . $shortcode. '.php' );
	}

	/* incluir novos shortcodes no template */
	$taxonomies = array(
		
	);
	foreach ( $taxonomies as $key => $tax  ) {
		require_once( LIBS_TAXONOMY . '/' . $tax. '.php' );
	}
?>