<?php

/* ---------------------------------------------------------------------------
 * Create new post type
 * --------------------------------------------------------------------------- */
function tratamentos_post_type() {  
    $labels = array(
        'name'                  => __('Tratamentos', THEME_NAME),
        'singular_name'         => __('Tratamento', THEME_NAME),
        'add_new'               => __('Add Novo',THEME_NAME),
        'add_new_item'          => __('Add Novo Item', THEME_NAME),
        'edit_item'             => __('Editar Item', THEME_NAME),
        'new_item'              => __('Novo Item', THEME_NAME),
        'view_item'             => __('Ver Item', THEME_NAME),
        'search_items'          => __('Buscar Item', THEME_NAME),
        'not_found'             => __('Nenhum Item encontrado', THEME_NAME),
        'not_found_in_trash'    => __('Nenhum Item encontrando na lixeira', THEME_NAME), 
        'parent_item_colon'     => ''
      );
        
    $args = array(
        'labels'                => $labels,
        'menu_icon'             => 'dashicons-plus-alt',
        'public'                => true,
        'publicly_queryable'    => true,
        'show_ui'               => true, 
        'query_var'             => true,
        'show_in_rest'          => true,
        'capability_type'       => 'post',
        'hierarchical'          => false,
        'menu_position'         => null,
        'rewrite'               => array( 'slug' => __('tratamento', THEME_NAME), 'with_front' => true ),
        'supports'              => array( 'editor', 'author', 'comments', 'excerpt', 'page-attributes', 'thumbnail', 'title', 'revisions', 'custom-fields' ),
    ); 
      
    register_post_type( 'tratamentos', $args );
    
    register_taxonomy( 'tratamentos_cat', 'tratamentos', array(
        'hierarchical'          => true,
        'label'                 =>  __('Categoria', THEME_NAME),
        'singular_label'        =>  __('Categoria', THEME_NAME),
        'rewrite'               => true,
        'query_var'             => true
    ));

}
add_action( 'init', 'tratamentos_post_type' );


/* ---------------------------------------------------------------------------
 * Edit columns
 * --------------------------------------------------------------------------- */
function tratamentos_edit_columns($columns){
    $newcolumns = array(
        "cb"                    => "<input type=\"checkbox\" />",
        "tratamentos_thumbnail" => __('Miniatura', THEME_NAME),
        "title"                 => __('Título', THEME_NAME),
        "tratamentos_cat"       => __('Categoria', THEME_NAME),
        "tratamentos_order"     => __('Ordem', THEME_NAME),
    );
    $columns = array_merge($newcolumns, $columns);  
    
    return $columns;
}
add_filter("manage_edit-tratamentos_columns", "tratamentos_edit_columns");  


/* ---------------------------------------------------------------------------
 * Custom columns
 * --------------------------------------------------------------------------- */
function tratamentos_custom_columns($column){
    global $post;
    switch ($column){
        case "tratamentos_thumbnail":
            if ( has_post_thumbnail() ) { the_post_thumbnail('post_type'); }
            break;  
        case "tratamentos_cat":
            echo get_the_term_list($post->ID, 'tratamentos_cat', '', ', ','');
            break;
        case "tratamentos_order":
            echo $post->menu_order;
            break;  
    }
}
add_action("manage_posts_custom_column",  "tratamentos_custom_columns"); 