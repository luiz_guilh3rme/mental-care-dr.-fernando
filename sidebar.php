<?php if ( is_active_sidebar( 'default' ) ) : ?>
	<div id="widget-area" class="widget-area" role="complementary">
		<?php dynamic_sidebar( 'default' ); ?>
	</div><!-- .widget-area -->
<?php endif; ?>