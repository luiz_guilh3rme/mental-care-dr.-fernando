<?php if (!is_page(array('contato', 'obrigado-pelo-seu-contato'))) : ?>
<div class="container form-contato">
    <h2 class="section--title center form-title"><strong> Pergunte ao Doutor</strong></h2>
    <p class="form-legend-alias">Preencha os campos abaixo e receba o primeiro atendimento.</p>
    <?php 
        echo do_shortcode('[contact-form-7 id="301" title="Contato - Doutor"]');
        ?>
</div>
<?php endif; ?>

<?php if( !is_front_page() ): ?>
<?php if ( is_active_sidebar( 'call-to-action' ) ) : ?>
<?php dynamic_sidebar( 'call-to-action' ); ?>
<?php endif; ?>
<?php endif; ?>

<footer id="footer" class="footer">
    <section class="container">

        <?php if ( is_active_sidebar( 'footer' ) ) : ?>
        <?php dynamic_sidebar( 'footer' ); ?>
        <?php endif; ?>

        <nav class="footer--menu">
            <ul>
                <!-- Menu Home -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( home_url() ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(2); ?></a>
                </li>
                <!-- Menu Sobre -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( get_permalink(64) ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(64); ?></a>
                </li>
                <!-- Menu Doenças -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( get_permalink(77) ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(77); ?></a>
                </li>
                <!-- Menu Tratamentos -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( get_permalink(83) ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(83); ?></a>
                </li>
                <!-- Menu Perguntas e Respostas -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( get_permalink(36) ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(36); ?></a>
                </li>
                <!-- Menu Blog -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( get_permalink(8) ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(8); ?></a>
                </li>
                <!-- Menu Contato -->
                <li class="footer--menu-item">
                    <a href="<?php echo esc_url( get_permalink(10) ); ?>" class="footer--menu-link">
                        <?php echo get_the_title(10); ?></a>
                </li>
            </ul>
        </nav>
    </section>

    <?php if ( is_active_sidebar( 'copyright' ) ) : ?>
    <?php dynamic_sidebar( 'copyright' ); ?>
    <?php endif; ?>

    <div class="mobile-cta">
        <a class="open-modal" target="_BLANK">
            <i class="fa fa-phone"></i>
            Nós te Ligamos
        </a>
        <a class="whatsapp-mobile" href="wpp" target="_BLANK">
            <i class="fa fa-whatsapp"></i>
            Nosso Whatsapp
        </a>
    </div>

    <div class="modal">
        <div class="form-wrapper">
            <?= do_shortcode('[contact-form-7 id="276" title="Nós te Ligamos"]'); ?>
            <button class="close-modal" aria-label="Fechar Modal">&times;</button>
        </div>
    </div>

    <div class="whatsapp">
        <button class="close-zap">&times;</button>
        <a class="link-whats whatspp-lateral" href="wpp" title="Fale conosco no Whatsapp!"
            target="_blank">
            <img src="<?php bloginfo('template_url'); ?>/images/cta-whatsapp.png" alt="Mande um Whatsapp!" class="sprite-cta-whatsapp">
        </a>
    </div>

</footer>

<!-- Scripts -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyDUDI1_0FsZAMZcJJH3WJ-laySAJO-iMZ4&sensor=true&amp;language=en"
    type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts-2.js" type="text/javascript"></script>
<?php wp_footer(); ?>
<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125977665-1"></script>

<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());
    gtag('config', 'UA-125977665-1');
</script>
</body>

</html>