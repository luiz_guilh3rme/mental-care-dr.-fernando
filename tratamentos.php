<?php 
/*
*	Template Name: Tratamentos
*/
?>

<?php get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<main class="interna">
			<section class="interna--banner" style="background-image: url('<?php the_post_thumbnail_url('full'); ?>');">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h2 class="interna--banner-title"><?php the_title(); ?></h2>
						</div>
						<div class="col-xs-12 col-sm-5 col-md-5">
							<div class="interna--banner-text"><?php the_excerpt(); ?></div>
							<a href="#interna-content" class="interna--banner-button scroll"><i class="fa fa-angle-down"></i></a>
						</div>
					</div>
				</div>
			</section>
			<section id="interna-content" class="container-fludi interna--contain">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
								
								<?php the_content(); ?>

								<?php 					
				                    $args = array(
				                        'post_type'      => 'tratamentos',
				                        'orderby'        => 'title', 
				                        'order'          => 'ASC',
				                        'posts_per_page' => -1
				                    );
				                   	$tratamentos = new WP_Query( $args );
								?>	
								<?php if($tratamentos->have_posts()) : ?>
									<ul class="list-icon row">
										<?php while($tratamentos->have_posts()) : $tratamentos->the_post(); ?>
											<li class="list-icon--item col-xs-12 col-sm-4 col-md-4">
												<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
													<i class="fa fa-heartbeat"></i>
													<p><?php the_title(); ?></p>
												</a>
											</li>
										<?php endwhile; ?>
									</ul>
								<?php endif; wp_reset_query(); ?>

							</div>
						</div>
					</div>
				</div>
			</section>
		</main>
	<?php endwhile; ?>

<?php get_footer(); ?>