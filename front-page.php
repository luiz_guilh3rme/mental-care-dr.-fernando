<?php get_header(); ?>
	
	<?php $Drs = new Drs(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		
		<?php if( have_rows( 'sliders' ) ) : ?>
			<section class="banner-home">
				<ul id="carousel-home">
					<?php while( have_rows( 'sliders' ) ) : the_row(); ?>
						<li class="item" style="background-image: url('<?php the_sub_field('imagem'); ?>');">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6">
										<?php if(get_sub_field('titulo')) : ?>
											<h2 class="banner-home--title"><?php echo get_sub_field('titulo'); ?></h2>
										<?php endif; ?>
										<?php if(get_sub_field('descricao')) : ?>
											<p class="banner-home--text"><?php echo get_sub_field('descricao'); ?></p>
										<?php endif; ?>
										<?php if(get_sub_field('link')) : ?>
											<a href="<?php echo get_sub_field('link'); ?>" class="banner-home--button"><?php echo __( 'Saiba mais', THEME_NAME ); ?> <i class="fa fa-angle-right"></i></a>
										<?php endif; ?>										
									</div>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			</section>
		<?php endif; ?>

		<section class="container-fluid section grey destaque-home">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-5 col-md-5">
							<?php if(get_field('sobre_imagem')) : ?>
								<img class="logo-home" src="<?php echo get_field('sobre_imagem'); ?>" alt="" />
							<?php endif; ?>								
						</div>
						<div class="col-xs-12 col-sm-7 col-md-7">
							<?php if(get_field('sobre_titulo')) : ?>
								<h1 class="section--title"><?php echo get_field('sobre_titulo'); ?></h1>
							<?php endif; ?>	
							<?php if(get_field('sobre_descricao')) : ?>
								<p><?php echo get_field('sobre_descricao'); ?></p>
							<?php endif; ?>	
							<?php if(get_field('sobre_link')) : ?>
								<a href="<?php echo get_field('sobre_link'); ?>" class="bt-medium"><?php echo __( 'Saiba mais', THEME_NAME ); ?> <i class="fa fa-angle-right"></i></a>
							<?php endif; ?>	
							<?php 
								$sociais = "";
						        $perfis = get_field('sobre_perfis_sociais');
						        if(isset($perfis) && strlen($perfis) > 0){
						            $perfis_array = explode("\n", $perfis);
						            $sociais .= '<ul class="social-list">';
						            foreach($perfis_array as $perfil) {
						                $sociais .= '
						                    <li class="social-list--item">
						                        <a href="'.$perfil.'" target="_blank title="'. $Drs->get_social_network_sherad_id($perfil).'" id="'.$Drs->get_social_network_sherad_id($perfil).'" class="social-list--link">
						                            <i class="fa fa-'.$Drs->get_social_network_sherad_id($perfil).'"></i>
						                        </a>
						                    </li>
						                ';
						            }
						            $sociais .= '</ul>';
						        }
						        echo $sociais; 
							?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php 					
            $args = array(
                'post_type'      => 'doencas',
                'orderby'        => 'title', 
                'order'          => 'ASC',
                'posts_per_page' => -1
            );
           	$doencas = new WP_Query( $args );
		?>	
		<?php if($doencas->have_posts()) : ?>
			<section class="container-fluid section">
				<div class="row">
					<div class="container">
						<?php if(get_field('titulo_secao_doecas')) : ?>
							<h2 class="section--title center"><?php echo get_field('titulo_secao_doecas'); ?></h2>
						<?php endif; ?>
						<ul class="carousel-doencas">
							<?php while($doencas->have_posts()) : $doencas->the_post(); ?>
								<li class="item">
									<div class="carousel-doencas--image">
										<?php if( get_field('imagem_carrosel_home') ) : ?>
											<img src="<?php echo get_field('imagem_carrosel_home'); ?>" alt="<?php esc_attr(get_the_title()); ?>" />
										<?php endif; ?>
									</div>
									<h3 class="carousel-doencas--title"><?php the_title(); ?></h3>
									<?php if( get_field('texto_carrosel_ome') ) : ?>
										<p class="carousel-doencas--text"><?php echo get_field('texto_carrosel_ome'); ?></p>
									<?php endif; ?>
									<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="carousel-doencas--link">
										<?php echo __( 'Saiba mais', THEME_NAME ); ?>
										<i class="fa fa-angle-right"></i>
									</a>
								</li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div>
			</section>
		<?php endif; wp_reset_query(); ?>

	<?php endwhile; ?>	

<?php get_footer(); ?>