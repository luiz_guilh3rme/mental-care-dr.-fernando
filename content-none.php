<section class="no-results not-found">

	<h1 class="page-title">
		<?php _e( 'Oops! Nenhum conteúdo encontrado.', THEME_NAME ); ?>
	</h1>

	<div class="page-content">

		<p><?php _e( 'Desculpe, mas nada correspondeu aos termos da sua pesquisa. Por favor, tente novamente com algumas palavras-chave diferentes.', THEME_NAME ); ?></p>
		<?php get_search_form(); ?>

	</div><!-- .page-content -->
</section><!-- .no-results -->
