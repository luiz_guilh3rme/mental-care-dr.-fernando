<?php $Drs = new Drs(); ?>

<?php get_header(); ?>

	<?php 
		$url = wp_get_attachment_image_src( get_post_thumbnail_id(8), 'full' );
		$src = $url[0];
	?>
	<main class="interna blog">
		<section class="interna--banner" style="background-image: url('<?php echo $src; ?>');">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<h2 class="interna--banner-title"><?php echo __( 'Resultado de busca', THEME_NAME ); ?></h2>
					</div>
				</div>
			</div>
		</section>

		<section id="interna-content" class="container-fludi interna--contain">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
							<div class="row">
								<aside class="col-xs-12 col-sm-4 col-md-4">									
									<form action="<?php echo site_url('/'); ?>" class="blog-search">
										<i class="fa fa-search"></i>
										<input type="search" name="s" class="blog-search--input" id="busca" placeholder="Pesquisar" />
                        				<input type="hidden" name="tipo-busca" value="any" />
									</form>
								</aside>
								<div class="col-xs-12 col-sm-8 col-md-8">						
									<ul class="cat-menu">
										<li class="cat-menu--item">
											<?php printf( __( '%s', THEME_NAME ), get_search_query() ); ?>
										</li>
									</ul>
								</div>
							</div>
							
							<?php if(isset($_GET['tipo-busca'])) : ?>								
								<?php 
									if($_GET['tipo-busca'] == 'post') :
										if ( !empty($_GET['s']) ) {
				                            $_busca = $_GET['s'] != '' ? $_GET['s'] : '';					                        
											query_posts(
												array(
													"post_type" => $_GET['tipo-busca'],
													's'         => $_busca,
													'posts_per_page' => -1
												) 
											);
										}
										if( have_posts() ) : 
									?>
										<?php while ( have_posts() ) : the_post(); ?>									    
											<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
												<div class="row">											
													<div class="col-xs-12 col-sm-5 col-md-5">
														<div class="image" style="background-image:url('<?php the_post_thumbnail_url('full'); ?>')">
															<img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php esc_attr(get_the_title()); ?>" />												
														</div>
													</div>
													<div class="col-xs-12 col-sm-7 col-md-7">
														<div class="contain">
															<h2 class="entry-title">
																<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
																	<?php the_title(); ?>
																</a>
															</h2>
															<div class="entry-summary">
																<?php the_excerpt(); ?>
															</div>
															<a href="<?php echo esc_url( get_permalink() ); ?>" class="banner-home--button">
																<?php echo __( 'Saiba mais', THEME_NAME ); ?> <i class="fa fa-angle-right"></i>
															</a>
															<div class="date"><?php echo get_the_date(); ?></div>
														</div>
													</div>
												</div>
											</article>
										<?php endwhile; ?>
									<?php else : ?>
										<p><?php echo __( 'Nenhum resultado econtrado com essa pesquisa.', THEME_NAME ); ?></p>
									<?php endif; ?>
								<?php endif; ?>

							<?php endif; ?>

							<?php if(isset($_GET['tipo-busca'])) : ?>								
								<?php 
									if($_GET['tipo-busca'] == 'any') :
										if ( !empty($_GET['s']) ) {
				                            $_busca = $_GET['s'] != '' ? $_GET['s'] : '';					                        
											query_posts(
												array(
													"post_type"      => $_GET['tipo-busca'],
													's'              => $_busca,
													'posts_per_page' => -1
												) 
											);
										}
										if( have_posts() ) : 
									?>
										<?php while ( have_posts() ) : the_post(); ?>									    
											<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
												<div class="row">											
													<div class="col-xs-12 col-sm-12 col-md-12">
														<div class="contain">
															<h2 class="entry-title">
																<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
																	<?php the_title(); ?>
																</a>
															</h2>
															<div class="entry-summary">
																<?php the_excerpt(); ?>
															</div>
															<a href="<?php echo esc_url( get_permalink() ); ?>" class="banner-home--button">
																<?php echo __( 'Saiba mais', THEME_NAME ); ?> <i class="fa fa-angle-right"></i>
															</a>
															<div class="date"><?php echo get_the_date(); ?></div>
														</div>
													</div>
												</div>
											</article>
										<?php endwhile; ?>
									<?php else : ?>
										<p><?php echo __( 'Nenhum resultado econtrado com essa pesquisa.', THEME_NAME ); ?></p>
									<?php endif; ?>
								<?php endif; ?>

							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</section>
	
	</main>

<?php get_footer(); ?>
