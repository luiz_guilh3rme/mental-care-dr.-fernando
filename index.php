<?php $Drs = new Drs(); ?>

<?php get_header(); ?>

	<?php 
		$url = wp_get_attachment_image_src( get_post_thumbnail_id(8), 'full' );
		$src = $url[0];
	?>
	<main class="interna">
		<section class="interna--banner" style="background-image: url('<?php echo $src; ?>');">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<h2 class="interna--banner-title"><?php single_post_title(); ?></h2>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5">
						<?php 
							if( get_field('editor', 8) ) :
								//echo '<div class="interna--banner-text">'.get_field('editor', 8).'</div>';
							endif;
						?>
						<a href="#interna-content" class="interna--banner-button scroll"><i class="fa fa-angle-down"></i></a>
					</div>
				</div>
			</div>
		</section>

		<section id="interna-content" class="container-fludi interna--contain">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-offset-1 col-sm-10 col-md-10">
							<div class="row">
								<aside class="col-xs-12 col-sm-4 col-md-4">									
									<form action="<?php echo site_url('/'); ?>" class="blog-search">
										<i class="fa fa-search"></i>
										<input type="search" name="s" class="blog-search--input" id="busca" placeholder="Pesquisar" />
										<input type="hidden" name="tipo-busca" value="post" />
									</form>
								</aside>
								<div class="col-xs-12 col-sm-8 col-md-8">									
									<?php $cats = get_categories( array('hide_empty' => 1) ); ?>
									<ul class="cat-menu">
										<?php foreach( $cats as $cat ) : ?>
											<li class="cat-menu--item">
												<a href="<?php echo get_category_link($cat->term_id); ?>" title="Ver todos os posts da categoria <?php echo $cat->name; ?>">
													<?php echo $cat->name; ?>
												</a>
											</li>
										<?php endforeach ?>
									</ul>
								</div>
							</div>
							<?php while ( have_posts() ) : the_post(); ?>

								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

									<div class="row">											
										<div class="col-xs-12 col-sm-5 col-md-5">
											<div class="image" style="background-image:url('<?php the_post_thumbnail_url('full'); ?>')">
												<img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php esc_attr(get_the_title()); ?>" />												
											</div>
										</div>
										<div class="col-xs-12 col-sm-7 col-md-7">
											<div class="contain">
												<h2 class="entry-title">
													<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
														<?php the_title(); ?>
													</a>
												</h2>

												<div class="entry-summary">
													<?php the_excerpt(); ?>
												</div>

												<a href="<?php echo esc_url( get_permalink() ); ?>" class="banner-home--button">
													<?php echo __( 'Saiba mais', THEME_NAME ); ?> <i class="fa fa-angle-right"></i>
												</a>

												<div class="date"><?php echo get_the_date(); ?></div>
											</div>
										</div>
									</div>							

								</article>

							<?php endwhile; ?>

							<div class="blog-pagination">
								<?php $Drs->init_pagination(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</main>

<?php get_footer(); ?>